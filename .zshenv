# export ZDOTDIR="$HOME"/.config/zsh
# source "$ZDOTDIR"/.zshenv

export XDG_CACHE_HOME="$HOME/.cache"
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"
export LANG="en_US.UTF-8"
export LC_ALL="en_US.UTF-8"
export EDITOR=nvim
export VISUAL=nvim
export BAT_THEME="Solarized (light)"

function prepend_path() {
    case ":$PATH:" in
        *":$1:"* | "$1:"*) :;;
        *) PATH="$1:$PATH"
    esac
}

prepend_path "/usr/local/sbin"
prepend_path "$HOME/.local/bin"
prepend_path "$HOME/sbin"
prepend_path "$HOME/bin"
export PATH

if [ -r "$ZDOTDIR/private.zshenv" ]; then
    source "$ZDOTDIR/private.zshenv"
fi
