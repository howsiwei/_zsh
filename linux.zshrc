export MY_CONDA_PREFIX="$HOME/conda"

alias l='ls -FG'
alias la='ls -AFG'
alias ll='ls -lFG'
alias lt='tree'
alias o='xdg-open'
