export MY_CONDA_PREFIX=/usr/local/miniconda3

alias E='/Applications/Emacs.app/Contents/MacOS/Emacs'
alias ls=exa
alias l=ls
alias la='ls -a'
alias ll='ls -l'
alias lt='ls -T'
alias o=open
alias chrome='/Applications/Google\ Chrome.app/Contents/MacOS/Google\ Chrome'
alias jabref='/Applications/JabRef.app/Contents/MacOS/JabRef --nogui'

function clip() {
    case "$1" in
    -i)
        pbcopy
        ;;
    -o)
        pbpaste
        ;;
    *)
        exit 1
        ;;
    esac
}
