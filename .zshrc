case "$OSTYPE" in
    linux*) source "$ZDOTDIR"/linux.zshrc;;
    darwin*) source "$ZDOTDIR"/osx.zshrc;;
esac

ZGEN_DIR="$XDG_DATA_HOME/zgen"
source "$ZGEN_DIR/zgen.zsh"

if ! zgen saved; then
    zgen loadall <<EOPLUGINS
        zsh-users/zsh-autosuggestions
        zsh-users/zsh-completions
        zsh-users/zsh-history-substring-search
        zsh-users/zsh-syntax-highlighting
        Tarrasch/zsh-autoenv
EOPLUGINS
    zgen save
fi

setopt noflowcontrol
setopt append_history
setopt share_history
setopt extended_history
setopt hist_ignore_dups
setopt hist_ignore_space
setopt hist_reduce_blanks
setopt hist_find_nodups
setopt hist_subst_pattern
setopt bang_hist
setopt hist_verify
setopt auto_cd
setopt interactivecomments
setopt null_glob

HISTFILE="$XDG_DATA_HOME"/zsh/zsh_history
SAVEHIST=10001000
HISTSIZE=10001000

PS1='%n@%m %~$ '

# WORDCHARS='*?_-.[]~=/&;!#$%^(){}<>'
WORDCHARS='*?_-.[]~!#$%^<>'
KEYTIMEOUT=1
bindkey -v
bindkey '^b' backward-char
bindkey '^f' forward-char
bindkey '^?' backward-delete-char
bindkey '^u' backward-kill-line
bindkey '^a' beginning-of-line
bindkey '^e' end-of-line
bindkey '^w' backward-kill-word
bindkey "^[[A" history-beginning-search-backward
bindkey "^[[B" history-beginning-search-forward
bindkey "^r" history-incremental-pattern-search-backward
bindkey "^s" history-incremental-pattern-search-forward
bindkey '^k' history-substring-search-up
bindkey '^o' history-substring-search-up
bindkey '^p' history-substring-search-up
bindkey '^n' history-substring-search-down
bindkey '^q' edit-command-line

function zle-keymap-select zle-line-init {
    local vicmd_seq=""
    local viins_seq=""
    case $TERM in
        dumb) ;;
        screen*)
            # In tmux, escape sequences to pass to terminal need to be
            # surrounded in a DSC sequence and double-escaped:
            vicmd_seq="\ePtmux;\e\e[2 q\e\\"
            viins_seq="\ePtmux;\e\e[6 q\e\\"
            ;;
        *)
            # If it's not tmux then can use normal sequences
            vicmd_seq="\e[2 q"
            viins_seq="\e[6 q"
    esac
    # Command mode
    if [ $KEYMAP = vicmd ]; then
        echo -ne $vicmd_seq
    else
        echo -ne $viins_seq
    fi
}

# Change appearance
zle -N zle-line-init      # When a new line starts
zle -N zle-keymap-select  # When vi mode changes

autoload edit-command-line
zle -N edit-command-line

alias e='emacsclient -n'
alias g=git
alias gh=hub
alias t=todo.sh
alias tok='tokei -c80'
alias v='$VISUAL -R'

function mcd() {
    mkdir "$@" && cd "$_"
}

compdef _mkdir mcd

function autoenv_source() {
    source .autoenv.zsh
}

function conda-setup() {
    prepend_path "$MY_CONDA_PREFIX/bin"
    eval "$("$MY_CONDA_PREFIX"/bin/conda shell.zsh hook 2> /dev/null)"
}

function ca() {
    conda-setup
    if [ $# -ne 0 ]; then
        conda activate "$@"
    else
        conda deactivate
    fi
}

function command_exists() {
    command -v "$1" > /dev/null
}

function opam-setup() {
    eval "$(opam env)"
}

function cargo-setup() {
    prepend_path "$HOME/.cargo/bin"
}

function cpan-setup() {
    eval "$(perl -I "$HOME/perl5/lib/perl5/" -Mlocal::lib)"
}

function prog-setup() {
    cpan-setup
    cargo-setup
    opam-setup
    conda-setup
}

function launch() {
    ( $* &> /dev/null & )
}

function p() {
    mpw "$@" | tr -d '\n' | clip -i
}
